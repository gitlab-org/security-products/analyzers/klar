#!/bin/sh

set -e

cd "$(dirname "$0")/.."

CLAIR_VERSION='2.1.4'
KLAR_EXECUTABLE_SHA=09764983d4e9a883754b55b16edf5f0be558ab053ad6ee447aca0199ced3d09f
KLAR_EXECUTABLE_VERSION=2.4.0

case $1 in
  bin)
    export CGO_ENABLED=0
    export GO111MODULE=on
    export GOARCH=amd64
    export GOOS=linux

    mkdir -p ./pkg/bin/
    go build \
      -ldflags="-X '$(go list -m)/metadata.AnalyzerVersion=$(./script/version)'" \
      -o ./pkg/bin/analyzer

    go build -o ./pkg/bin/clair github.com/quay/clair/v2/cmd/clair

    curl -s "https://github.com/optiopay/klar/releases/download/v${KLAR_EXECUTABLE_VERSION}/klar-${KLAR_EXECUTABLE_VERSION}-linux-amd64" -Lo ./pkg/bin/klar
    echo "${KLAR_EXECUTABLE_SHA}  ./pkg/bin/klar" | sha256sum -c
    chmod +x ./pkg/bin/klar
    ;;

  pkg)
    rm -rf ./pkg
    mkdir -p ./pkg ./pkg/config ./pkg/script ./pkg/vendor/
    ./script/build bin
    cp config/* ./pkg/config/
    cp script/*.sh ./pkg/script/
    cp LICENSE ./pkg
    cp .dockerignore ./pkg

    # Source is included to bundle project licenses.
    git clone --depth 1 \
      --branch \
      "v$KLAR_EXECUTABLE_VERSION" https://github.com/optiopay/klar.git \
      ./pkg/vendor/github.com/optiopay/klar/

    git clone --depth 1 \
      --branch \
      "v${CLAIR_VERSION}" https://github.com/quay/clair.git \
      ./pkg/vendor/github.com/quay/clair/
    ;;

  image)
    if command -v docker > /dev/null; then
      IMAGE_NAME=${IMAGE_NAME:-klar:latest}
      export DOCKER_BUILDKIT=1
      docker build \
        --network=host \
        --pull \
        --force-rm \
        --build-arg CLAIR_VERSION=$CLAIR_VERSION \
        --build-arg KLAR_EXECUTABLE_VERSION=$KLAR_EXECUTABLE_VERSION \
        --file ./Dockerfile \
        -t "$IMAGE_NAME" .
    else
      echo "Install docker: https://docs.docker.com/engine/installation/"
      exit 1
    fi
    ;;

  *)
    ./script/build pkg
    ./script/build image
    ;;
esac
