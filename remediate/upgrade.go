package remediate

import (
	"fmt"
	"strings"
)

type osNotFoundError struct {
	operatingSystem string
}

func (e osNotFoundError) Error() string {
	return fmt.Sprintf("No remediation handler available for Operating System: '%s'", e.operatingSystem)
}

func generateUpgradeCmd(namespaceName, featureName, fixedBy string) (string, error) {
	switch operatingSystem(namespaceName) {
	case "debian", "ubuntu":
		return debianUpdatePackage(featureName), nil
	case "centos", "oracle":
		return rhelUpdatePackage(featureName), nil
	case "alpine":
		return alpineUpdatePackage(featureName, fixedBy), nil
	}

	return "", osNotFoundError{namespaceName}
}

func operatingSystem(namespaceName string) string {
	osName := strings.Split(namespaceName, ":")[0]
	return strings.ToLower(osName)
}

func debianUpdatePackage(packageName string) string {
	return fmt.Sprintf("apt-get update && apt-get upgrade -y %s && rm -rf /var/lib/apt/lists/*", packageName)
}

func rhelUpdatePackage(packageName string) string {
	return fmt.Sprintf(`yum -y check-update || { rc=$?; [ "$rc" -neq 100 ] && exit $rc; yum update -y %s; } && yum clean all`, packageName)
}

func alpineUpdatePackage(packageName, fixedBy string) string {
	return fmt.Sprintf("apk --no-cache update && apk --no-cache add %s=%s", packageName, fixedBy)
}
