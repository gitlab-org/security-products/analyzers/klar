package allowlist_test

import (
	"reflect"
	"strings"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/allowlist"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/internal/testutils"
)

func TestParseAllowlistFile(t *testing.T) {
	var cases = []struct {
		name, filePath string
	}{
		{"when there are only two unapproved CVEs with new file format", "clair-whitelist.yml"},
		{"when there are only two unapproved CVE with old file formats", "vulnerability-allowlist.yml"},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {

			got, err := allowlist.Parse(testutils.FixturesPath(tc.filePath))
			if err != nil {
				t.Errorf("Expected no err: %s", err)
			}

			want := allowlist.Allowlist{GeneralAllowlist: map[string]string{
				"CVE-2019-8696":  "cups",
				"CVE-2014-8166":  "cups",
				"CVE-2017-18248": "cups",
			},
				Images: map[string]map[string]string{
					"registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256": {
						"CVE-2018-4180": "cups",
					},
					"your.private.registry:5000/centos": {
						"RHSA-2015:1419": "libxml2",
						"RHSA-2015:1447": "grep",
					},
				},
			}

			if !reflect.DeepEqual(want, got) {
				t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
			}
		})
	}

	t.Run("when the path to the allowlist does not exist", func(t *testing.T) {
		// empty allowlist should be returned
		want := allowlist.Allowlist{}
		got, err := allowlist.Parse(testutils.FixturesPath("non-existent-allowlist.yml"))

		if err != nil {
			t.Errorf("Expected no err: %s", err)
		}

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})

	t.Run("when the allowlist does not have the correct format", func(t *testing.T) {
		// an error should be returned
		_, err := allowlist.Parse(testutils.FixturesPath("invalid-allowlist.yml"))
		if err == nil {
			t.Error("Expected err, got nil")
		}

		want := "Unable to parse allowlist file with path"
		got := err.Error()

		if !strings.Contains(got, want) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})
}

func TestRemoveDockerImageTag(t *testing.T) {
	t.Run("when the registry is private", func(t *testing.T) {
		want := "some.private.registry:5000/centos"
		got := allowlist.RemoveDockerImageTag("some.private.registry:5000/centos:6.6")

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})

	t.Run("when the registry is public", func(t *testing.T) {
		want := "registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256"
		got := allowlist.RemoveDockerImageTag("registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256:bc09fe2e0721dfaeee79364115aeedf2174cce0947b9ae5fe7c33312ee019a4e")

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})

	t.Run("when the registry does not have a version tag", func(t *testing.T) {
		want := "registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256"
		got := allowlist.RemoveDockerImageTag("registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256")

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})
}

func TestGetImageVulnerabilities(t *testing.T) {
	allowlistTmp := map[string]map[string]string{
		"some.private.registry:5000/centos": map[string]string{
			"RHSA-2017:2832": "nss-tools",
			"RHSA-2015:1419": "libxml2",
		},
		"non-matching-image-name": map[string]string{"RHSA-2018:2180": ""},
	}

	t.Run("when a matching image is found", func(t *testing.T) {
		want := map[string]string{
			"RHSA-2017:2832": "nss-tools",
			"RHSA-2015:1419": "libxml2",
		}

		got := allowlist.GetImageVulnerabilities("some.private.registry:5000/centos:6.6", allowlistTmp)

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})

	t.Run("when a matching image is not found", func(t *testing.T) {
		var want map[string]string

		got := allowlist.GetImageVulnerabilities("another.private.registry:5000/alpine:latest", allowlistTmp)

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})
}

func TestCheckForUnapprovedVulnerabilities(t *testing.T) {
	allowlistTmp := allowlist.Allowlist{
		GeneralAllowlist: map[string]string{
			"RHSA-2015:1634": "sqlite",
			"RHSA-2016:1626": "python",
			"RHSA-2019:2471": "openssl",
		},
		Images: map[string]map[string]string{
			"some.private.registry:5000/centos": map[string]string{
				"RHSA-2015:1419": "libxml2",
			},
			"non-matching-image-name": map[string]string{"RHSA-2018:2180": ""},
		},
	}

	report := &issue.Report{
		Version: issue.CurrentVersion(),
		Vulnerabilities: []issue.Issue{
			{
				CompareKey:  "centos:6:sqlite:RHSA-2015:1634",
				Identifiers: []issue.Identifier{{Value: "RHSA-2015:1634"}},
				Severity:    issue.SeverityLevelMedium,
			},
			{
				CompareKey:  "centos:6:nss:RHSA-2017:2832",
				Identifiers: []issue.Identifier{{Value: "RHSA-2017:2832"}},
				Severity:    issue.SeverityLevelHigh,
			},
			{
				CompareKey:  "centos:6:libxml2:RHSA-2015:1419",
				Identifiers: []issue.Identifier{{Value: "RHSA-2015:1419"}},
				Severity:    issue.SeverityLevelHigh,
			},
		},
	}

	t.Run("when a vulnerability matches only the general allowlist", func(t *testing.T) {
		// it should strip out only a single approved vulnerability
		want := map[string]bool{
			"centos:6:nss:RHSA-2017:2832":     true,
			"centos:6:libxml2:RHSA-2015:1419": true,
		}

		got := allowlist.CheckForUnapprovedVulnerabilities("registry.gitlab.com/centos:6.6", report, allowlistTmp)

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})

	t.Run("when vulnerability matches are found in the general and image allowlist", func(t *testing.T) {
		// it should strip out both approved vulnerabilities
		want := map[string]bool{
			"centos:6:nss:RHSA-2017:2832": true,
		}

		got := allowlist.CheckForUnapprovedVulnerabilities("some.private.registry:5000/centos:6.6", report, allowlistTmp)

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})
}
