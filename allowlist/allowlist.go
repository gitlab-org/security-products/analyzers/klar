package allowlist

import (
	"os"
	"path/filepath"
	"strings"

	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/command"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/reporter"
)

// TODO: remove all references to deprecatedAllowlist
// https://gitlab.com/gitlab-org/gitlab/-/issues/224655
type deprecatedAllowlist struct {
	GeneralAllowlist map[string]string            //[key: CVE and value: CVE description]
	GeneralWhitelist map[string]string            //[key: CVE and value: CVE description]
	Images           map[string]map[string]string // image name with [key: CVE and value: CVE description]
}

// Allowlist format is specified here:
// https://gitlab.com/gitlab-org/security-products/analyzers/klar/-/blob/master/testdata/vulnerability-allowlist.yml
type Allowlist struct {
	GeneralAllowlist map[string]string            //[key: CVE and value: CVE description]
	Images           map[string]map[string]string // image name with [key: CVE and value: CVE description]
}

// Parse reads the allowlist file and parses it
func Parse(allowlistPath string) (Allowlist, error) {
	allowlist := Allowlist{}
	deprecatedAllowlist := deprecatedAllowlist{}

	if deprecatedAllowlistPath := checkDeprecatedAllowlistFile(); deprecatedAllowlistPath != "" {
		allowlistPath = deprecatedAllowlistPath
	}

	if _, err := os.Stat(allowlistPath); os.IsNotExist(err) {
		log.Warnf("Allowlist file with path '%s' does not exist, skipping", allowlistPath)
		return allowlist, nil
	}

	f, err := os.Open(allowlistPath)
	if err != nil {
		return allowlist, errors.Wrapf(err, "unable to open allowlist file with path '%s'", allowlistPath)
	}
	defer f.Close()

	if err = yaml.NewDecoder(f).Decode(&deprecatedAllowlist); err != nil {
		return allowlist, errors.Wrapf(err, "Unable to parse allowlist file with path '%s', could not decode.",
			allowlistPath)
	}

	allowlist.GeneralAllowlist = deprecatedAllowlist.GeneralAllowlist

	if deprecatedAllowlist.GeneralWhitelist != nil {
		allowlist.GeneralAllowlist = deprecatedAllowlist.GeneralWhitelist
	}
	allowlist.Images = deprecatedAllowlist.Images

	log.Infof("Found allowlist file with path '%s'", allowlistPath)

	return allowlist, nil
}

// CheckForUnapprovedVulnerabilities checks if the found vulnerabilities are approved or not in the allowlist
// Returns a map where the key is the CompareKey of the unapproved vulnerabilities
func CheckForUnapprovedVulnerabilities(dockerImageName string, report *issue.Report,
	allowlist Allowlist) map[string]bool {

	unapproved := map[string]bool{}
	imageVulnerabilities := getImageVulnerabilities(dockerImageName, allowlist.Images)

	for _, vulnerability := range report.Vulnerabilities {
		vulnerable := true
		extID := reporter.ExtIDForVulnerability(vulnerability)

		//Check if the vulnerability exists in the GeneralAllowlist
		if vulnerable {
			if _, exists := allowlist.GeneralAllowlist[extID]; exists {
				vulnerable = false
			}
		}

		//If not in GeneralAllowlist check if the vulnerability exists in the imageVulnerabilities
		if vulnerable && len(imageVulnerabilities) > 0 {
			if _, exists := imageVulnerabilities[extID]; exists {
				vulnerable = false
			}
		}
		if vulnerable {
			unapproved[vulnerability.CompareKey] = true
		}
	}
	return unapproved
}

// removeDockerImageTag takes as input a docker image path with a tag and returns only the image portion
// without including the tag details
//
// Examples:
//
// given `some.private.registry:5000/gl-centos:6.6`
// returns `some.private.registry:5000/gl-centos`
//
// given `centos:6.6`
// returns `centos`
func removeDockerImageTag(dockerImageName string) string {
	// if the dockerImageName doesn't contain an image tag version, then just return
	// the dockerImageName
	if !strings.Contains(dockerImageName, ":") {
		return dockerImageName
	}

	dockerImageNameComponents := strings.Split(dockerImageName, ":")

	// check if the image name has the form some.private.registry:5000/gl-centos:6.6
	if len(dockerImageNameComponents) > 2 {
		return strings.Join(dockerImageNameComponents[0:2], ":")
	}

	return dockerImageNameComponents[0]
}

// getImageVulnerabilities returns image specific allowlist of vulnerabilities from allowlistImageVulnerabilities
func getImageVulnerabilities(dockerImageName string,
	allowlistImageVulnerabilities map[string]map[string]string) map[string]string {

	var imageVulnerabilities map[string]string

	if val, exists := allowlistImageVulnerabilities[removeDockerImageTag(dockerImageName)]; exists {
		imageVulnerabilities = val
	}
	return imageVulnerabilities
}

func checkDeprecatedAllowlistFile() string {
	oldAllowListPath := filepath.Join(os.Getenv(command.EnvVarCIProjectDir), "clair-whitelist.yml")

	if _, err := os.Stat(oldAllowListPath); os.IsNotExist(err) {
		log.Warnf("Allowlist file with path '%s' does not exist, skipping", oldAllowListPath)
		return ""
	}

	log.Warnf("DEPRECATION NOTICE: Detected deprecated 'clair-whitelist.yml' configuration file. Please rename this file to 'vulnerability-allowlist.yml'.")

	return oldAllowListPath
}
