package clair_test

import (
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"testing"
	"time"

	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/clair"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/environment"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/internal/testutils"
)

func TestWriteConfigFile(t *testing.T) {
	tempDir := testutils.TempDir()
	defer testutils.CleanUp()

	t.Run("WriteConfigFile", func(t *testing.T) {
		*clair.PathToConfigFileTemplateP = testutils.FixturesPath("clair-config.yaml.template")
		*clair.PathToConfigFileP = filepath.Join(tempDir, "clair-config.yaml")

		err := clair.WriteConfigFile(testutils.DefaultClairVulnerabilitiesDBConnectionString)
		if err != nil {
			t.Errorf("Expected no err: %s", err)
		}

		got, err := testutils.ReadFile(*clair.PathToConfigFileP)
		if err != nil {
			t.Fatalf("Expected no err: %s", err)
		}

		want, err := testutils.ReadFile(testutils.FixturesPath("clair-config.yaml"))
		if err != nil {
			t.Fatalf("Expected no err: %s", err)
		}

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})
}

func TestDBConnectionString(t *testing.T) {
	var cases = []struct {
		Name, ClairVulnerabilitiesDBHost, ClairVulnerabilitiesDBConnectionString, KubernetesPort, ExpectedConnectionString string
	}{
		{
			"when both clairVulnerabilitiesDBHost and clairVulnerabilitiesDBConnectionString have a custom value and kubernetes is not configured",
			"custom-db-host", "custom-db-connection-string", "",
			"postgresql://postgres:password@custom-db-host:5432/postgres?sslmode=disable&statement_timeout=60000",
		},
		{
			"when both clairVulnerabilitiesDBHost and clairVulnerabilitiesDBConnectionString have a custom value and kubernetes is configured",
			"custom-db-host", "custom-db-connection-string", "8080",
			"postgresql://postgres:password@custom-db-host:5432/postgres?sslmode=disable&statement_timeout=60000",
		},
		{
			"when clairVulnerabilitiesDBHost is set to an empty string and clairVulnerabilitiesDBConnectionString has a custom setting and kubernetes is not configured",
			"", "postgresql://postgres:password@custom-db-connection-string:5432", "",
			"postgresql://postgres:password@custom-db-connection-string:5432",
		},
		{
			"when clairVulnerabilitiesDBHost is set to an empty string and clairVulnerabilitiesDBConnectionString has a custom setting and kubernetes is configured",
			"", "postgresql://postgres:password@custom-db-connection-string:5432", "8080",
			"postgresql://postgres:password@custom-db-connection-string:5432",
		},
		{
			"when clairVulnerabilitiesDBHost is left as the default value and clairVulnerabilitiesDBConnectionString has a custom setting and kubernetes is not configured",
			testutils.DefaultClairVulnerabilitiesDBHost,
			"postgresql://postgres:password@custom-db-connection-string:5432", "",
			"postgresql://postgres:password@custom-db-connection-string:5432",
		},
		{
			"when clairVulnerabilitiesDBHost is left as the default value and clairVulnerabilitiesDBConnectionString has a custom setting and kubernetes is configured",
			testutils.DefaultClairVulnerabilitiesDBHost,
			"postgresql://postgres:password@custom-db-connection-string:5432", "8080",
			"postgresql://postgres:password@custom-db-connection-string:5432",
		},
		{
			"when both clairVulnerabilitiesDBHost and clairVulnerabilitiesDBConnectionString are left as the default values and kubernetes is not configured",
			testutils.DefaultClairVulnerabilitiesDBHost,
			testutils.DefaultClairVulnerabilitiesDBConnectionString, "",
			testutils.DefaultClairVulnerabilitiesDBConnectionString,
		},
		{
			"when both clairVulnerabilitiesDBHost and clairVulnerabilitiesDBConnectionString are left as the default values and kubernetes is configured",
			testutils.DefaultClairVulnerabilitiesDBHost,
			testutils.DefaultClairVulnerabilitiesDBConnectionString, "8080",
			"postgresql://postgres:password@127.0.0.1:5432/postgres?sslmode=disable&statement_timeout=60000",
		},
	}

	for _, tc := range cases {
		t.Run(tc.Name, func(t *testing.T) {
			if tc.KubernetesPort != "" {
				os.Setenv("KUBERNETES_PORT", tc.KubernetesPort)
				defer testutils.UnsetEnvVars(t)
			}

			connectionString := clair.DBConnectionString(tc.ClairVulnerabilitiesDBHost,
				tc.ClairVulnerabilitiesDBConnectionString, testutils.DefaultClairVulnerabilitiesDBHost,
				testutils.DefaultClairVulnerabilitiesDBConnectionString)

			if connectionString != tc.ExpectedConnectionString {
				t.Errorf("Expected string '%s', got '%s'", tc.ExpectedConnectionString, connectionString)
			}
		})
	}
}

func TestClairServerArgs(t *testing.T) {
	t.Run("when DockerInsecure is false", func(t *testing.T) {
		*clair.PathToConfigFileP = "/clair-config.yaml"

		want := []string{"-config=/clair-config.yaml"}
		got := clair.ClairServerArgs()

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})

	t.Run("when DockerInsecure is true", func(t *testing.T) {
		os.Setenv(environment.EnvVarDockerInsecure, "true")
		defer testutils.UnsetEnvVars(t)
		*clair.PathToConfigFileP = "/clair-config.yaml"

		want := []string{"-config=/clair-config.yaml", "-insecure-tls"}
		got := clair.ClairServerArgs()

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})
}

func TestStartBackgroundServer(t *testing.T) {
	os.Setenv(environment.EnvVarBypassVulnerabilitiesDBServerCheck, "true")
	defer testutils.UnsetEnvVars(t)

	tempDir := testutils.TempDir()
	defer testutils.CleanUp()

	*clair.PathToConfigFileTemplateP = testutils.FixturesPath("clair-config.yaml.template")
	*clair.PathToConfigFileP = filepath.Join(tempDir, "clair-config.yaml")
	*clair.PathToClairBinaryP = testutils.FixturesPath("mocks/clairmock.sh")

	t.Run("when the clair server is started successfully", func(t *testing.T) {
		err := clair.StartBackgroundServer(true, testutils.DefaultClairVulnerabilitiesDBConnectionString)
		if err != nil {
			t.Errorf("Expected no err: %s", err)
		}
	})

	t.Run("when the clair server is not started successfully becuase the number of retries "+
		"surpasses the maximum number allowed", func(t *testing.T) {
		*clair.ClairRetryWaitP = time.Duration(time.Nanosecond)
		*clair.MaxClairServerRetryCountP = 1

		err := clair.StartBackgroundServer(true, testutils.DefaultClairVulnerabilitiesDBConnectionString)
		if err == nil {
			t.Fatal("Expected err, got nil")
		}

		got := err.Error()
		want := fmt.Sprintf("error while waiting for Clair API to start. Giving up after %d retries",
			*clair.MaxClairServerRetryCountP)

		if want != got {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})

	t.Run("when the clair server can't be started because the path to the binary does not exist", func(t *testing.T) {
		*clair.PathToClairBinaryP = "/non-existent-path"

		err := clair.StartBackgroundServer(true, testutils.DefaultClairVulnerabilitiesDBConnectionString)
		if err == nil {
			t.Fatal("Expected err, got nil")
		}

		got := err.Error()
		want := "An error occurred while starting the clair server process: fork/exec /non-existent-path: no such file or directory"

		if want != got {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})
}
