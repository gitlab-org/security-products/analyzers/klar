package environment_test

import (
	"os"
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/environment"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/internal/testutils"
)

func TestDockerImageName(t *testing.T) {
	var cases = []struct {
		Name    string
		EnvVars map[string]string
		WantStr string
		WantErr error
	}{
		{
			"when a DOCKER_IMAGE env var is set",
			map[string]string{"DOCKER_IMAGE": "expected-docker-image-name"},
			"expected-docker-image-name",
			nil,
		},
		{
			"when the CI_APPLICATION_REPOSITORY/CI_APPLICATION_TAG env vars are set",
			map[string]string{
				"CI_APPLICATION_REPOSITORY": "expected-docker-image-name",
				"CI_APPLICATION_TAG":        "expected-docker-image-tag",
			},
			"expected-docker-image-name:expected-docker-image-tag",
			nil,
		},
		{
			"when only the CI_APPLICATION_REPOSITORY env var has been set",
			map[string]string{
				"CI_APPLICATION_REPOSITORY": "expected-docker-image-name",
			},
			"",
			environment.ErrApplicationTagOrCommitShaNotProvided,
		},
		{
			"when the CI_APPLICATION_REPOSITORY and CI_COMMIT_SHA env vars have been set",
			map[string]string{
				"CI_APPLICATION_REPOSITORY": "expected-docker-image-name",
				"CI_COMMIT_SHA":             "expected-docker-image-tag",
			},
			"expected-docker-image-name:expected-docker-image-tag",
			nil,
		},
		{
			"when an empty string has been passed no environment variables have been set",
			map[string]string{},
			"",
			environment.ErrDockerImageNotProvided,
		},
		{
			"when only the CI_REGISTRY_IMAGE env var has been set",
			map[string]string{
				"CI_REGISTRY_IMAGE": "expected-docker-image-name",
			},
			"",
			environment.ErrDockerImageNotProvided,
		},
		{
			"when only the CI_COMMIT_REF_SLUG env var has been set",
			map[string]string{
				"CI_COMMIT_REF_SLUG": "expected-docker-image-tag",
			},
			"",
			environment.ErrDockerImageNotProvided,
		},
		{
			"when both the CI_REGISTRY_IMAGE and CI_COMMIT_REF_SLUG and env vars have been set but the CI_COMMIT_SHA has not been passed",
			map[string]string{
				"CI_REGISTRY_IMAGE":  "expected-docker-image-name",
				"CI_COMMIT_REF_SLUG": "expected-docker-image-tag",
			},
			"",
			environment.ErrCommitShaNotProvided,
		},
		{
			"when the CI_REGISTRY_IMAGE, CI_COMMIT_REF_SLUG and CI_COMMIT_SHA env vars have been set",
			map[string]string{
				"CI_REGISTRY_IMAGE":  "expected-docker-image-name",
				"CI_COMMIT_REF_SLUG": "expected-docker-image-tag",
				"CI_COMMIT_SHA":      "expected-ci-commit-sha",
			},
			"expected-docker-image-name/expected-docker-image-tag:expected-ci-commit-sha",
			nil,
		},
	}

	for _, tc := range cases {
		t.Run(tc.Name, func(t *testing.T) {
			testutils.UnsetEnvVars(t)

			for envVarName, envVarValue := range tc.EnvVars {
				os.Setenv(envVarName, envVarValue)
			}

			got, err := environment.DockerImageName()

			if tc.WantErr != nil {
				if err != tc.WantErr {
					t.Errorf("Expected err: %s, got %s", tc.WantErr, err)
				}
			}

			if !reflect.DeepEqual(tc.WantStr, got) {
				t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", tc.WantStr, got)
			}
		})
	}
}
