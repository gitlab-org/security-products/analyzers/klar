package environment

// Export for testing
var (
	ErrDockerImageNotProvided               = errDockerImageNotProvided
	ErrApplicationTagOrCommitShaNotProvided = errApplicationTagOrCommitShaNotProvided
	ErrCommitShaNotProvided                 = errCommitShaNotProvided
)
