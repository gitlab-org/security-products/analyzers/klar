# frozen_string_literal: true

module FixtureFileHelper
  def fixture_file_content(path, data = {})
    fixture_file(path).read
  end

  def fixture_file(path)
    Pathname.pwd.join("spec/fixtures/#{path}")
  end
end

RSpec.configure do |config|
  config.include(FixtureFileHelper)
end
