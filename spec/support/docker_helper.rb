# frozen_string_literal: true

RSpec.configure do |config|
  config.before(:suite) do
    Docker.new(pwd: Pathname.pwd).build(tag: "klar:latest")
  end
end
