# frozen_string_literal: true

RSpec.describe 'RHEL' do
  context "when scanning a RHEL based image" do
    include_examples "as container scanner"

    let(:env) do
      {
        DOCKERFILE_PATH: project.virtual_path.join("centos8-Dockerfile"),
        DOCKER_IMAGE: "centos:8"
      }
    end

    it "shells out to `rpm` on FIPS enabled hosts" do
      expect(docker.run(project: project, env: env, command: 'rpm -q rpm')).to be(true)
    end
  end
end
