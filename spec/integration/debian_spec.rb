# frozen_string_literal: true

RSpec.describe 'Debian' do
  context "when scanning a Debian based image" do
    include_examples "as container scanner"

    let(:env) do
      {
        DOCKERFILE_PATH: project.virtual_path.join("debian-buster-Dockerfile"),
        DOCKER_IMAGE: "debian:buster"
      }
    end
  end
end
