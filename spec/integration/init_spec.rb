# frozen_string_literal: true

RSpec.describe '/usr/bin/init' do
  context "when running the docker image directly" do
    subject { Open3.capture3({}, "docker run #{Docker::DEFAULT_IMAGE_NAME}") }

    let(:stdout) { subject[0] }
    let(:stderr) { subject[1] }
    let(:status) { subject[2] }

    specify { expect(stdout).to be_empty }
    specify { expect(stderr).to include("Docker image value must be provided") }
    specify { expect(status).not_to be_success }
  end
end
